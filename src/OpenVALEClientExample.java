/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


public class OpenVALEClientExample {

static int GoldenEarsID = 0;
static int DioticID = 0;
static int WhiteNoiseID = 0;
static int NoiseGeneratorID = 0;
static InputStream inStream = null;
static OutputStream outStream = null;
static OpenVALEClient client;
static int totalTrials = 10;
static int trialNum = 0;
static int spkID = 0;

static ArrayList<Integer> sourceList = new ArrayList<>();
public static void main(String[] args) throws IOException {


        client = new OpenVALEClient("127.0.0.1",43201,true);
        try {


            ArrayList<Float> center = new ArrayList<>();
            center.add(0.0f);
            center.add(2.0f);
            center.add(0.0f);

            //NoiseGeneratorID = client.AddAudioSource(OpenVALEClient.NoiseType.wav,center,"gettysburgaddresssamwaterston.wav");
            GoldenEarsID = client.LoadHRTF("GoldenClusterMean_Snow_SH6E150.slh");
            System.out.println(GoldenEarsID);
            
            //NoiseGeneratorID = client.AddAudioSource(OpenVALEClient.NoiseType.wav,center,GoldenEarsID,"richardnixoncheckers.wav");//client.AddAudioSource(OpenVALEClient.NoiseType.noise,center,GoldenEarsID);
            //WhiteNoiseID = client.AddAudioSource(OpenVALEClient.NoiseType.noise,center,GoldenEarsID);
            WhiteNoiseID = client.AddAudioSource(OpenVALEClient.NoiseType.noise,center,GoldenEarsID);
            //WhiteNoiseID = 1;
            client.ShowFreeCursor("hand", true);
            client.DefineFront();
            System.out.println(client.SendNonStandardMessage("getsubjectnumber()"));
            //Thread.sleep((100000));
            client.StartRendering();
            //client.EnableSrc(WhiteNoiseID, true);
            //client.SendNonStandardMessage("rewindsource(0)");
            //client.EnableSrc(WhiteNoiseID  , true);
            //client.MuteAudioSource(WhiteNoiseID , false);
            
            client.EnableSrc(WhiteNoiseID, true);
            client.MuteAudioSource(WhiteNoiseID, false);
            client.DefineFront();
            client.ShowSnappedCursor("head", true);
            client.DisplayMessage("Something displayed");
            
             ArrayList<Float> led  = new ArrayList<>();
            led.add(0.0f);
            led.add(1.0f);
            led.add(0.0f);
            ArrayList<Float> testMask = new ArrayList<Float>();
            testMask.add(0f);
            testMask.add(1f);
            testMask.add(0f);
            center.clear();
            center.add(.0f);
            center.add(0.0f);
            center.add(0.0f);
            client.HighlightLocation(274, led);
            //client.EnableSrc(WhiteNoiseID, false);
            //client.AdjustSourcePosition(WhiteNoiseID, center);
            center.clear();
            center.add(0.0f);
            center.add(-2.0f);
            center.add(0.0f);
            //client.AdjustSourcePosition(NoiseGeneratorID, center);
                
            Thread.sleep(5000);
         
            //client.MuteAudioSource(WhiteNoiseID , true);
            //client.EnableSrc(WhiteNoiseID, false);
            //client.SendNonStandardMessage("rewindsource(0)");
            Thread.sleep(8000);
            //client.EnableSrc(WhiteNoiseID  , true);
            //client.MuteAudioSource(WhiteNoiseID , false);
            //client.AdjustOverallLevel( 20);
            //client.EnableSrc(WhiteNoiseID, true);   
            //client.MuteAudioSource(WhiteNoiseID , false);
            //client.EnableSrc(WhiteNoiseID, true);
            Thread.sleep(11000);
            client.Disconnect();
            /*
            client.AdjustSourceLevel(WhiteNoiseID,15);
            client.MuteAudioSource(WhiteNoiseID, false);
            client.AdjustSourcePosition(WhiteNoiseID, center);
            
            
            ArrayList<Float> led = new ArrayList<>();
            led.add(-0.455060f);
            led.add(-0.199758f);
            led.add(-0.867765f);
            
            ArrayList<Float> testMask = new ArrayList<Float>();
            testMask.add(1f);
            testMask.add(1f);
            testMask.add(0f);
            testMask.add(0f);
            led = client.GetSpeakerPosition(274);
            System.out.println(led.get(0) + ","+led.get(1)+ ","+led.get(2));
            
            led = client.GetSpeakerPosition(276);
            System.out.println(led.get(0) + ","+led.get(1)+ ","+led.get(2));
            
            led = client.GetSpeakerPosition(260);
            System.out.println(led.get(0) + ","+led.get(1)+ ","+led.get(2));
            
            led = client.GetSpeakerPosition(100);
            System.out.println(led.get(0) + ","+led.get(1)+ ","+led.get(2));
            
            led = new ArrayList<>();
            led.add(1.0f);
            led.add(0.0f);
            led.add(1.0f);
            client.HighlightLocation(275, led);
            
            led = new ArrayList<>();
            led.add(1.0f);
            led.add(0.0f);
            led.add(1.0f);
            testMask = new ArrayList<Float>();
            testMask.add(0f);
            testMask.add(1f);
            testMask.add(0f);
            client.HighlightLocation(testMask, led);
            */
            
            //client.SetLEDs(led, testMask);
            //client.ShowSnappedCursor("head", true);
            //client.WaitforResponse();
            //System.out.println("done");
            //Thread.sleep(1000);
            
            //client.ShowSnappedCursor("head", false);
            //Thread.sleep(1000);
            //client.ShowSnappedCursor("head", true);
            //Thread.sleep(1000);
            //client.ShowSnappedCursor("head", false);
            Thread.sleep(1000);
            //client.WaitForRecenter(274, .1f  );
            //Thread.sleep(1000);
            //client.AdjustSourcePosition(WhiteNoiseID, center);
            /*
            Random r = new Random();
            spkID = r.nextInt(277)+1;
            ArrayList<Integer> mask = new ArrayList<>();
            ArrayList<Float> color = new ArrayList<>();

            mask.clear();
            mask.add(1);
            mask.add(1);
            mask.add(1);
            mask.add(1);

            color.clear();
            color.add(1f);
            color.add(0.0f);
            color.add(1f);


            client.ShowSnappedCursor("hand",true);
            for(int trialCount = 0 ; trialCount<totalTrials; trialCount++){
                trialNum = trialCount;

                color.clear();
                color.add(1f);
                color.add(1f);
                color.add(1f);
                playTrial(WhiteNoiseID);
                //playTrial(NoiseGeneratorID );

                client.HighlightLocation(spkID,color);
                spkID = r.nextInt(72)+1;
                OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
                client.HighlightLocation();
            

            }
*/

        }catch (java.io.IOException e){




        } catch (InterruptedException ex) {
        Logger.getLogger(OpenVALEClientExample.class.getName()).log(Level.SEVERE, null, ex);
    }

    }
    static void playTrial(int srcID){
        try {

            client.EnableSrc(srcID, true);
            client.AdjustSourcePosition(WhiteNoiseID,spkID);
            client.MuteAudioSource(srcID, false);
            OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
            client.MuteAudioSource(srcID, true);
            client.EnableSrc(srcID, false);
        }
        catch(Exception e){



        }


    }
}
