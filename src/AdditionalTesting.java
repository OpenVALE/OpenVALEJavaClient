
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
Additional testing of OpenVALE functions
 */

public class AdditionalTesting {
    
    public static void main(String[] args) throws InterruptedException, IOException {
        
        OpenVALEClient client = new OpenVALEClient("127.0.0.1", 43201, true);
        
        try {
            int hrtfNo = client.LoadHRTF("GoldenClusterMean_Snow_SH6E150.slh");
            
            ArrayList<Float> position = new ArrayList();
            position.add(0f);
            position.add(0f);
            position.add(0f);
            
            int sourceNo = client.AddAudioSource(OpenVALEClient.NoiseType.noise, position, hrtfNo);
            
            client.DefineFront(); //Zero everything out (based on init pos)
            client.ShowFreeCursor("head", true); //Puts landmark on board
            
            //Start audio rendering
            client.StartRendering();
            client.EnableSrc(sourceNo, true);
            client.MuteAudioSource(sourceNo, false);
            
            Thread.sleep(5000);
            
            //Start playing with sound
//            for (int i = 0; i >= -50; i--) {
//                System.out.println(client.AdjustOverallLevel(i));
//                Thread.sleep(500);
//            }
//            
//            Thread.sleep(5000);
//            
//            for (int i = -50; i <= 0; i++) {
//                client.AdjustOverallLevel(i);
//                Thread.sleep(500);
//            }

            //Test ability to get head position
//            while (true) {
//                System.out.println(client.GetHeadOrientation());
//            }

            //Play with users and sound sources
            ArrayList<Float> color = new ArrayList();
            color.add(0f);
            color.add(0f);
            color.add(1f);
            
            client.HighlightLocation(client.GetNearestSpeaker(position), color);
            client.WaitForRecenter(client.GetNearestSpeaker(position), 0.1f);
            
//            while (true) {
//                OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
//                if (response.speakerID == client.GetNearestSpeaker(position)) {
//                    client.DisplayMessage(Float.toString(response.responseTime));
//                    break;
//                }
//            }
            
            //To make the program pause and continue playing audio
            //client.WaitforResponse();
            
        } catch (IOException ex) {
            Logger.getLogger(AdditionalTesting.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
}
