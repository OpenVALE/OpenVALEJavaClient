/*
Test of functions included in the OpenVALE project
*/

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OpenVALEJavaTestClient {
    
    private static final int PAUSETIME = 3000;
    
    public static void main(String[] args) throws InterruptedException, IOException {
        
        OpenVALEClient client = null;
        
        try {
            /////////////////////
            //--INITIAL SETUP--//
            /////////////////////
            
            //OpenVALE client, with verbose mode turned on for debugging (used throughout)
            client = new OpenVALEClient("localhost", 43201, true);
            
            //Load test HRTF into the client (used thorughout)
            int hrtfNo = client.LoadHRTF("GoldenClusterMean_Snow_SH6E150.slh");
            
            //Static location, used thorughout for testing purposes
            ArrayList<Float> location = new ArrayList();
            location.add(1f);
            location.add(2f);
            location.add(3f);

            //Add audio sources to the client
            int noiseSourceNo = client.AddAudioSource(OpenVALEClient.NoiseType.noise, location, hrtfNo);
            int wavSourceNo = client.AddAudioSource(OpenVALEClient.NoiseType.wav, location, hrtfNo, "soundTone.wav");
            
            //Initiate testing
            client.DisplayMessage("--FIRE TO BEGIN TEST SCRIPT--");
            client.WaitforResponse();
            
            //Redefine "front" (setting origin based on HMD initial position)
            client.DefineFront();
            
            //Begin audio rendering
            client.StartRendering();
            
            client.MuteAudioSource(noiseSourceNo, true);
            client.MuteAudioSource(wavSourceNo, true);

            ///////////////////////////////////////////////////
            //--TESTING BEGINNING WITH A NOISE AUDIO SOURCE--//
            ///////////////////////////////////////////////////

            client.DisplayMessage("AUDIO TESTING--NOISE SOURCE");
            Thread.sleep(PAUSETIME);
            
            //Test audio playback
            basicAudioTest(client, noiseSourceNo);
 
            //Test volume control
            volumeTest(client, noiseSourceNo);
            
            //Test changing HRTF
            setHRTFTest(client, noiseSourceNo);
           
            //Test position control
            //ELIMINATED FROM FUTURE STANDARD TESTING

            /////////////////////////////////////////////////
            //--TESTING BEGINNING WITH A WAV AUDIO SOURCE--//
            /////////////////////////////////////////////////
            
            client.DisplayMessage("AUDIO TESTING--WAV SOURCE");
            Thread.sleep(PAUSETIME);
            
            //Test audio playback
            basicAudioTest(client, wavSourceNo);
 
            //Test volume control
            volumeTest(client, wavSourceNo);
            
            //Test changing HRTF
            setHRTFTest(client, wavSourceNo);
           
            //Test position control
            //ELIMINATED FROM FUTURE STANDARD TESTING
            
            ////////////////////////////////////////////////////////
            //--TESTING USER INTERACTION WITH THE ALF SIMULATION--//
            ////////////////////////////////////////////////////////
            
            //Test of basic user interaction
            basicInteractionTest(client);
            
            //Test of location information methods
            locationInformationTest(client);
            
            //Test of LED lighting in ALF
            ledLightingTest(client, noiseSourceNo, location);
            
            //Test of speaker highlighting in ALF
            speakerHighlightingTest(client, noiseSourceNo, location);
            
            //Test of waitForRecenter-based interaction with simulation
            waitForRecenterTest(client, noiseSourceNo, location);
            
            //Test of getSubjectNumber
            getSubjectNumberTest(client);
            
            client.DisplayMessage("--TEST SCRIPT COMPLETE--");
            
            client.Reset();
            
            client.DisplayMessage("");

            } catch (IOException ex) {
                Logger.getLogger(OpenVALEJavaTestClient.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("ERROR HAS OCCURRED");
                client.DisplayMessage("FATAL: ERROR HAS OCCURRED");
                System.exit(1);
        }
        
    }
        
    private static void basicAudioTest(OpenVALEClient client, int sourceNo) throws IOException, InterruptedException {
        client.DisplayMessage("TESTING BASIC AUDIO PLAYBACK");

        //Enable the audio source
        client.EnableSrc(sourceNo, true);

        //Unmute audio source
        client.MuteAudioSource(sourceNo, false);

        //Add artificial pause to let audio play
        Thread.sleep(PAUSETIME);

        //Mute audio source
        client.MuteAudioSource(sourceNo, true);

        //Disable the audio source
        client.EnableSrc(sourceNo, false);
        
        //Add artificial pause to ensure audio muted
        Thread.sleep(2000);
        
        client.DisplayMessage("BASIC AUDIO PLAYBACK TEST COMPLETE");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
    }
    
    private static void volumeTest(OpenVALEClient client, int sourceNo) throws IOException, InterruptedException {
        client.DisplayMessage("TESTING VOLUME CONTROL");
        Thread.sleep(PAUSETIME);
            
        //Enable audio
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);
        
        client.DisplayMessage("AdjustOverallLevel");
        Thread.sleep(PAUSETIME);

        //Test decreasing volume
        for (int i = 0; i >= -60; i -= 20) {
            client.DisplayMessage("Volume Changed " + i + " dB");
            client.AdjustOverallLevel(i);
            Thread.sleep(2000);
        }

        //Test increasing volume
        for (int i = -40; i <= 0; i += 20) {
            client.DisplayMessage("Volume Changed " + i + " dB");
            client.AdjustOverallLevel(i);
            Thread.sleep(2000);
        }
        
        client.DisplayMessage("AdjustSourceLevel");
        Thread.sleep(2000);
        
        //Set audio source to a fixed speaker location
        client.AdjustSourcePosition(sourceNo, 10);
        
        for (int i = 0; i >= -60; i -= 20) {
            client.DisplayMessage("Volume Changed " + i + " dB");
            client.AdjustSourceLevel(sourceNo, i);
            Thread.sleep(2000);
        }

        //Test increasing volume
        for (int i = -40; i <= 0; i += 20) {
            client.DisplayMessage("Volume Changed " + i + " dB");
            client.AdjustSourceLevel(sourceNo, i);
            Thread.sleep(2000);
        }
        
        //Disable audio
        client.MuteAudioSource(sourceNo, true);
        client.EnableSrc(sourceNo, false);

        client.DisplayMessage("VOLUME CONTROL TESTING COMPLETE");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
    }
    
    @Deprecated
    private static void positionTest(OpenVALEClient client, int sourceNo, ArrayList location) throws IOException, InterruptedException {
        client.DisplayMessage("BEGIN POSITION CONTROL TEST");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
            
        //Adjust location (from [0,0,0] to [1,2,3])
        location.clear();
        location.add(1f);
        location.add(2f);
        location.add(3f);

        //Move client to new location
        client.AdjustSourcePosition(sourceNo, location);

        //Re-enable audio
        client.DisplayMessage("AdjustSourcePosition--source location");
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);

        //Artificial pause to let audio play
        Thread.sleep(3000);

        //Move client to new speaker location
        client.DisplayMessage("AdjustSourcePosition--speaker ID");
        client.AdjustSourcePosition(sourceNo, 10);

        //Artificial pause to let audio play
        Thread.sleep(3000);

        //Disable audio
        client.MuteAudioSource(sourceNo, true);
        client.EnableSrc(sourceNo, false);

        client.DisplayMessage("POSITION CONTROL TEST COMPLETE");
        Thread.sleep(PAUSETIME);
    }
    
    private static void basicInteractionTest(OpenVALEClient client) throws IOException, InterruptedException {
        client.DisplayMessage("TESTING BASIC USER INTERACTION");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);

        //Test to ensure free cursor, hand mode stays in place
        client.ShowFreeCursor("hand", true);
        client.DisplayMessage("Ensure free cursor follows hand. Fire when done.");
        client.WaitforResponse();

        //Recenter the cursor in the field of vision
        client.DefineFront();

        //Test to ensure free cursor, head mode moves with head
        client.ShowFreeCursor("hand", false);
        client.ShowFreeCursor("head", true);
        client.DisplayMessage("Ensure free cursor follows head. Fire when done.");
        client.WaitforResponse();

        client.DefineFront();

        //Test to ensure snapped cursor, hand mode, stays in place
        client.ShowFreeCursor("head", false);
        client.ShowSnappedCursor("hand", true);
        client.DisplayMessage("Ensure snapped cursor follows hand. Fire when done.");
        client.WaitforResponse();

        client.DefineFront();

        //Test to ensure snapped cursor, head mode, moves with head
        client.ShowSnappedCursor("hand", false);
        client.ShowSnappedCursor("head", true);
        client.DisplayMessage("Ensure snapped cursor follows head. Fire when done.");
        client.WaitforResponse();

        client.DefineFront();
        client.ShowSnappedCursor("head", false);

        client.DisplayMessage("BASIC USER INTERACTION TEST COMPLETE");
    }
    
    private static void locationInformationTest(OpenVALEClient client) throws IOException, InterruptedException {
        client.DisplayMessage("TEST OF LOCATION INFORMATION METHODS");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);

        //Show cursor to help user navigate
        client.DefineFront();
        client.ShowFreeCursor("head", true);
        
        //Tests head orientation is returned
        client.DisplayMessage("Fire to see current head coordinates");
        client.WaitforResponse();
        client.DisplayMessage("[" + client.GetHeadOrientation().get(0).toString() + " "
            + client.GetHeadOrientation().get(1).toString() + " "
            + client.GetHeadOrientation().get(2).toString() + "]"
            + "...Fire to continue...");
        
        //Pause to let user see message
        client.WaitforResponse();
        
        //Tests head orientation is returned when head moves
        client.DisplayMessage("Move head and fire to see new head coordinates");
        client.WaitforResponse();
        client.DisplayMessage("[" + client.GetHeadOrientation().get(0).toString() + " "
            + client.GetHeadOrientation().get(1).toString() + " "
            + client.GetHeadOrientation().get(2).toString() + "]"
            + "...Fire to continue...");
        
        //Artificial pause to let user see message
        client.WaitforResponse();

        //Tests 6DOF is returned
        client.DisplayMessage("Fire to see current 6DOF");
        client.WaitforResponse();
        client.DisplayMessage("[" + client.GetHead6DOF().get(0).toString() + " "
            + client.GetHead6DOF().get(1).toString() + " "
            + client.GetHead6DOF().get(2).toString() + "] ["
            + client.GetHead6DOF().get(3).toString() + " "
            + client.GetHead6DOF().get(4).toString() + " "
            + client.GetHead6DOF().get(5).toString() + "]"
            + "...Fire to continue...");
        
        //Artificial pause to let user see message
        client.WaitforResponse();
        
        //Tests 6DOF is returned
        client.DisplayMessage("Move head and fire to see new 6DOF");
        client.WaitforResponse();
        client.DisplayMessage("[" + client.GetHead6DOF().get(0).toString() + " "
            + client.GetHead6DOF().get(1).toString() + " "
            + client.GetHead6DOF().get(2).toString() + "] ["
            + client.GetHead6DOF().get(3).toString() + " "
            + client.GetHead6DOF().get(4).toString() + " "
            + client.GetHead6DOF().get(5).toString() + "]"
            + "...Fire to continue...");
        
        //Artificial pause to let user see message
        client.WaitforResponse();
        
        //Switch cursor between head and hand tests
        client.ShowFreeCursor("head", false);
        client.DefineFront();
        client.ShowFreeCursor("hand", true);
        
        //Gets the position of the speaker nearest to the one clicked
        client.DisplayMessage("Fire to see coordinates of speaker nearest to"
                + " the cursor");
        OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
        client.DisplayMessage("[" + client.GetSpeakerPosition(response.speakerID).get(0).toString() + " "
            + client.GetSpeakerPosition(response.speakerID).get(1).toString() + " "
            + client.GetSpeakerPosition(response.speakerID).get(2).toString() + "]"
            + "...Fire to continue...");
        
        //Artificial pause to let user see message
        client.WaitforResponse();
        
        //Gets the position of the speaker nearest to the one clicked
        client.DisplayMessage("Move over new speaker and fire to see nearest"
            + " speaker coordinates");
        response = client.WaitforResponse();
        client.DisplayMessage("[" + client.GetSpeakerPosition(response.speakerID).get(0).toString() + " "
            + client.GetSpeakerPosition(response.speakerID).get(1).toString() + " "
            + client.GetSpeakerPosition(response.speakerID).get(2).toString() + "]"
            + "...Fire to continue...");
        
        //Artificial pause to let user see message
        client.WaitforResponse();

        //Ensures gets nearest speaker to head orientation
        client.DisplayMessage("Fire to see number of speaker nearest to the cursor");
        response = client.WaitforResponse();
        client.DisplayMessage(Integer.toString(client.GetNearestSpeaker(client.GetSpeakerPosition(response.speakerID)))
            + "...Fire to continue...");
        
        //Artificial pause to let user see message
        client.WaitforResponse();
        
        //Ensures gets nearest speaker to head orientation
        client.DisplayMessage("Move over new speaker and fire to see new speaker number");
        response = client.WaitforResponse();
        client.DisplayMessage(Integer.toString(client.GetNearestSpeaker(client.GetSpeakerPosition(response.speakerID)))
            + "...Fire to continue...");
        
        //Artificial pause to let user see message
        client.WaitforResponse();

        //Hide cursor before further tests
        client.ShowFreeCursor("head", false);

        client.DisplayMessage("TEST OF LOCATION INFORMATION METHODS COMPLETE");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
    }
    
    private static void ledLightingTest(OpenVALEClient client, int sourceNo, ArrayList location) throws IOException, InterruptedException {
        client.DisplayMessage("TEST OF LED LIGHTING IN ALF SIMULATION");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
        
        //Test LED lighting in ALF using position parameter
        client.DisplayMessage("TEST LED LIGHT--POSITION");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
        
        client.DefineFront();
        client.ShowFreeCursor("hand", true);
        
        String locNoComma = location.toString().replaceAll(",", "");
        client.DisplayMessage("Speaker number " + client.GetNearestSpeaker(location)
                + " near location " + locNoComma +
                " is lit by the LED. Select speaker to verify this is true");

        ArrayList<Integer> ledMask = new ArrayList();
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        client.SetLEDs(location, ledMask);

        client.AdjustSourcePosition(sourceNo, client.GetNearestSpeaker(location));
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);

        while (true) {
            OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
            if (response.speakerID == client.GetNearestSpeaker(location)) {
                ledMask.clear();
                ledMask.add(0);
                ledMask.add(0);
                ledMask.add(0);
                ledMask.add(0);
                client.SetLEDs(location, ledMask);
                client.MuteAudioSource(sourceNo, true);
                client.EnableSrc(sourceNo, false);
                client.DisplayMessage("Speaker selected: " + response.speakerID 
                    + "...Fire to continue");
                client.WaitforResponse();
                break;
            }
        }

        //Test LED lighting in ALF using speaker ID parameter
        client.DisplayMessage("TEST LED LIGHT--SPEAKER ID");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
        
        client.DisplayMessage("Speaker number 10 is lit by the LED. Select "
            + "speaker to verify this is true");

        client.AdjustSourcePosition(sourceNo, 10);
        ledMask.clear();
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        client.SetLEDs(10, ledMask);

        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);

        while (true) {
            OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
            if (response.speakerID == 10) {
                ledMask.clear();
                ledMask.add(0);
                ledMask.add(0);
                ledMask.add(0);
                ledMask.add(0);
                client.SetLEDs(10, ledMask);
                client.MuteAudioSource(sourceNo, true);
                client.EnableSrc(sourceNo, false);
                client.DisplayMessage("Speaker selected: " + response.speakerID 
                    + "...Fire to continue");
                client.WaitforResponse();
                break;
            }
        }
        
        client.ShowFreeCursor("hand", false);
        
        client.DisplayMessage("TEST OF LED LIGHTING COMPLETE");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
    }
    
    private static void speakerHighlightingTest(OpenVALEClient client, int sourceNo, ArrayList location) throws IOException, InterruptedException {
        client.DisplayMessage("TEST OF SPEAKER HIGHLIGTING IN ALF SIMULATION");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);

        //Showing the free cursor to help user locate speaker
        client.ShowFreeCursor("hand", true);

        client.DisplayMessage("TEST SPEAKER HIGHLIGHTING--POSITION");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
        
        String locNoComma = location.toString().replaceAll(",", "");
        client.DisplayMessage("Speaker number " + client.GetNearestSpeaker(location)
                + " near location " + locNoComma +
                " is highlighted in green. Select speaker to verify this is true");
        
        //Test speaker highlighting in ALF using position parameter
        ArrayList<Float> color = new ArrayList();
        color.add(0f);
        color.add(1f);
        color.add(0f);
        client.HighlightLocation(client.GetSpeakerPosition(client.GetNearestSpeaker(location)), color);

        client.AdjustSourcePosition(sourceNo, client.GetNearestSpeaker(location));
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);

        while (true) {
            OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
            if (response.speakerID == client.GetNearestSpeaker(location)) {
                client.HighlightLocation();
                client.MuteAudioSource(sourceNo, true);
                client.EnableSrc(sourceNo, false);
                client.DisplayMessage("Speaker selected: " + response.speakerID 
                    + "...Fire to continue");
                client.WaitforResponse();
                break;
            }
        }
        
        client.DisplayMessage("TEST SPEAKER HIGHLIGHTING--SPEAKER ID");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
        
        client.DisplayMessage("Speaker number 10 is highlighted in green. Select "
            + "speaker to verify this is true");

        //Test speaker highlighting in ALF using speaker ID parameter
        client.HighlightLocation(10, color);
        client.AdjustSourcePosition(sourceNo, 10);
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);
        
        while (true) {
            OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
            if (response.speakerID == 10) {
                client.HighlightLocation();
                client.MuteAudioSource(sourceNo, true);
                client.EnableSrc(sourceNo, false);
                client.DisplayMessage("Speaker selected: " + response.speakerID 
                    + "...Fire to continue");
                client.WaitforResponse();
                break;
            }
        }

        client.MuteAudioSource(sourceNo, true);
        client.EnableSrc(sourceNo, false);

        //Disable the cursor before further testing
        client.ShowFreeCursor("hand", false);

        client.DisplayMessage("TEST OF SPEAKER HIGHLIGHTING COMPLETE");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
    }
    
    private static void waitForRecenterTest(OpenVALEClient client, int sourceNo, ArrayList location) throws IOException, InterruptedException {
        client.DisplayMessage("INTERACTION THROUGH WAITFORRECENTER TESTING");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);

        //Allow user to see cursor for navigation
        client.ShowFreeCursor("hand", true);
        
        client.DisplayMessage("WAITFORRECENTER--POSITION");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);

        //Turn on LEDs for active speaker
        ArrayList<Integer> ledMask = new ArrayList();
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        client.SetLEDs(location, ledMask);

        //Enable audio for active speaker
        client.AdjustSourcePosition(sourceNo, client.GetNearestSpeaker(location));
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);

        String locNoComma = location.toString().replaceAll(",", "");
        client.DisplayMessage("Speaker number " + client.GetNearestSpeaker(location)
                + " near location " + locNoComma +
                " is lit by the LED. Hover over this speaker to continue");

        client.WaitForRecenter(client.GetSpeakerPosition(client.GetNearestSpeaker(location)), 0.5f);

        //Turn off LEDs for active speaker after it has been found by user
        ledMask.clear();
        ledMask.add(0);
        ledMask.add(0);
        ledMask.add(0);
        ledMask.add(0);
        client.SetLEDs(location, ledMask);

        client.MuteAudioSource(sourceNo, true);
        
        //Display success message after finding source
        client.DisplayMessage("Success!");
        Thread.sleep(2000);

        client.DisplayMessage("WAITFORRECENTER--SPEAKER ID");

        //Re-activate LEDs for user after highlighting another speaker
        ledMask.clear();
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        ledMask.add(1);
        client.SetLEDs(10, ledMask);

        //Play audio from new location
        client.AdjustSourcePosition(sourceNo, 10);
        client.MuteAudioSource(sourceNo, false);

        client.DisplayMessage("Speaker number 10 is lit by the LED. Hover over "
            + "speaker to continue");

        client.WaitForRecenter(10, 0.2f);

        //Disable audio after user finds source
        client.MuteAudioSource(sourceNo, true);
        client.EnableSrc(sourceNo, false);
        
        //Turn off LEDs for active speaker after it has been found by user
        ledMask.clear();
        ledMask.add(0);
        ledMask.add(0);
        ledMask.add(0);
        ledMask.add(0);
        client.SetLEDs(10, ledMask);
        
        //Display success message after finding the source
        client.DisplayMessage("Success!");
        Thread.sleep(2000);

        //Turn off the free cursor before exiting method
        client.ShowFreeCursor("hand", false);

        client.DisplayMessage("INTERACTION THROUGH WAITFORRECENTER TESTING COMPLETE");
        
        //Artificial pause to let user see message
        Thread.sleep(PAUSETIME);
    }
    
    private static void getSubjectNumberTest(OpenVALEClient client) throws IOException, InterruptedException {
        client.DisplayMessage("GETSUBJECTNUMBER TESTING");
        Thread.sleep(PAUSETIME);
        client.DisplayMessage("");

        //Give client a cursor to use with num pad
        //client.ShowFreeCursor("hand", true);

        client.GetSubjectNumber();

        //Pause between calls to get sub number
        client.DisplayMessage("Enter another subject ID");
        Thread.sleep(PAUSETIME);
        client.DisplayMessage("");

        client.GetSubjectNumber();
        
        //Remove cursor when done
        //client.ShowFreeCursor("hand", false);

        client.DisplayMessage("GETSUBJECTNUMBER TESTING COMPLETE");
    }
    
    private static void setHRTFTest(OpenVALEClient client, int sourceNo) throws InterruptedException, IOException {
        client.DisplayMessage("SETHRTF TESTING");
        Thread.sleep(PAUSETIME);
        
        //Start playing sound
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);
        client.DisplayMessage("Using HRTF: GoldenClusterMean_Snow_SH6E150.slh");
        Thread.sleep(PAUSETIME);

        //Disable sound, switch HRTF
        client.EnableSrc(sourceNo, false);
        client.MuteAudioSource(sourceNo, true);
        int temp = client.LoadHRTF("GoldenClusterMean_SH6E100_HD280.slh");
        //client.StartRendering();
        client.SetHRTF(sourceNo, temp);
        
        //Play sound with new HRTF
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);
        client.DisplayMessage("Using HRTF: GoldenClusterMean_SH6E100_HD280.slh");
        Thread.sleep(PAUSETIME);
        
        //Revert to original HRTF
        client.EnableSrc(sourceNo, false);
        client.MuteAudioSource(sourceNo, true);
        temp = client.LoadHRTF("GoldenClusterMean_Snow_SH6E150.slh");
        client.SetHRTF(sourceNo, temp);
        
        //Play sound with original HRTF
        client.EnableSrc(sourceNo, true);
        client.MuteAudioSource(sourceNo, false);
        client.DisplayMessage("Using HRTF: GoldenClusterMean_Snow_SH6E150.slh");
        Thread.sleep(PAUSETIME);
        
        //End sound playback
        client.MuteAudioSource(sourceNo, true);
        client.EnableSrc(sourceNo, false);
        
        client.DisplayMessage("SETHRTF TESTING COMPLETE");
        Thread.sleep(PAUSETIME);
    }
        
}
