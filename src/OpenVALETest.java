/*
Open VALE Test File
--Localization test client
*/

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OpenVALETest {
    
    public static void main(String[] args) throws InterruptedException, IOException {
        
        OpenVALEClient client = new OpenVALEClient("127.0.0.1", 43201, true); //OV server, verbose
        
        try {
            //Load HRTF (printing ret val indicating HRTF)
            int HRTFNo = client.LoadHRTF("GoldenClusterMean_Snow_SH6E150.slh");
            
            //Prompt user to start
            client.DisplayMessage("--FIRE TO BEGIN--");
            client.WaitforResponse();
            client.DisplayMessage("");
            
            //Initial define front
            client.DefineFront();
            
            //Prompt for subject number
            client.GetSubjectNumber();
            
            client.ShowFreeCursor("hand", true);
            
            //Generate good speaker location
            boolean badFound = true;
            int speaker = 0;
            
            while (badFound) {
                speaker = (int) Math.ceil(Math.random() * 277);
                int[] badSpks = {60, 183, 208, 61, 252, 177, 37, 258, 202, 227, 36, 59, 62, 233};
                for (int badSpk : badSpks) {
                    if (speaker == badSpk) {
                        badFound = true;
                        break;
                    }
                    
                    badFound = false;
                }
            }
            
            //Initialize location to some random place
            ArrayList<Float> location = client.GetSpeakerPosition(speaker);

            //Add the audio source
            int sourceID = client.AddAudioSource(OpenVALEClient.NoiseType.noise, location, HRTFNo);
            
            client.StartRendering();
            
            //Keep track of how many are right
            int correct = 0;
            
            for (int i = 0; i < 20; i++) {
                //play
                client.EnableSrc(sourceID, true);
                client.MuteAudioSource(sourceID, false);
                
                client.DisplayMessage("Trial " + (i + 1) + " of 20. Click on noise source.");
                
                //"Burst" sound every other trial
                if (i % 2 == 0) {
                    Thread.sleep(500);
                    client.MuteAudioSource(sourceID, true);
                    client.EnableSrc(sourceID, false);
                }
                
                ArrayList<Float> green = new ArrayList();
                green.add(0.0f);
                green.add(1.0f);
                green.add(0.0f);
                
                ArrayList<Float> red = new ArrayList();
                red.add(1.0f);
                red.add(0.0f);
                red.add(0.0f);

                //wait
                OpenVALEClient.LocalizationResponse response = client.WaitforResponse();

                if (response.speakerID == client.GetNearestSpeaker(location)) {
                    //Congratulate user
                    client.DisplayMessage("Correct! Fire to continue.");
                    client.HighlightLocation(client.GetNearestSpeaker(location), green);
                    correct++;
                    
                    //mute orig src
                    client.EnableSrc(sourceID, false);
                    client.MuteAudioSource(sourceID, true);
                    client.WaitforResponse();
                } else {
                    //Mute orig source
                    client.MuteAudioSource(sourceID, true);
                    client.EnableSrc(sourceID, false);
                    
                    //Highlight wrong spk in
                    client.DisplayMessage("You clicked speaker number " 
                        + response.speakerID + "... ");
                    client.HighlightLocation(response.speakerID, red);
                    Thread.sleep(1500);

                    //Highlight right spk in green
                    client.DisplayMessage("...but the actual speaker was number "
                        + client.GetNearestSpeaker(location) + ". Fire to continue");
                    client.HighlightLocation(client.GetNearestSpeaker(location), green);
                    client.WaitforResponse();
                }
                
                //Make user recenter
                client.DisplayMessage("Move back to front of simulation and click"
                    + " location highlighted in green");
                client.HighlightLocation(274, green);
                while (i != 19) {
                    response = client.WaitforResponse();
                    if (response.speakerID == 274) {
                        client.HighlightLocation();
                        client.DefineFront();
                        break;
                    }
                }
                
                //Clear highlights
                client.HighlightLocation();
                
                //Move Noise source
                location = client.GetSpeakerPosition((int) Math.ceil(Math.random() * 277));
                client.AdjustSourcePosition(sourceID, client.GetNearestSpeaker(location));
            }
            
            //Re-hide free cursor
            client.ShowFreeCursor("hand", false);
            
            //Display done
            client.DisplayMessage("Complete! " + correct + " out of 20 correct!");
            
            //Reset the connection
            client.Reset();
            
        } catch (IOException ex) {
            Logger.getLogger(OpenVALETest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
