
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jason.Ayers
 */
public class SpatialServerClient {
    static int GoldenEarsID = 0;
    static int DioticID = 0;
    static String WhiteNoiseID = "";
    static int NoiseGeneratorID = 0;
    static Socket connection;
    static InputStream inStream = null;
    static OutputStream outStream = null;
    //static OpenVALEClient client;
    static int totalTrials = 10;
    static int trialNum = 0;
    static int spkID = 0;
    static String ipAddress = "127.0.0.1";
    static int port = 1112;
    static ArrayList<Integer> sourceList = new ArrayList<>();
    static BufferedReader connectionReader;
    static CharBuffer buffer = CharBuffer.allocate(256);
    public static void main(String[] args)  {
  
        try {
            connection = new Socket(ipAddress,port);
            inStream = connection.getInputStream();
            outStream = connection.getOutputStream();
            connectionReader = new BufferedReader(new InputStreamReader(inStream));
            String message;
            int replyLength;
            String reply;
            //message = "allocSigGenSrc N,1.0,100,0,1";
            //outStream.write((message +(char)3).getBytes());
            //outStream.flush();
            //replyLength = connectionReader.read(buffer);
            //String returnedID = buffer.split(",")[1];
            //buffer.rewind();
            //String reply = buffer.toString();
            //buffer.clear();
            
            //WhiteNoiseID = reply.trim().split(",")[2].replace(";",String.valueOf("").trim());
            WhiteNoiseID = "0";
            message = "enableSrc " + WhiteNoiseID + ",1";
            outStream.write((message +(char)3).getBytes());
            outStream.flush();
            replyLength = connectionReader.read(buffer);
            //String returnedID = buffer.split(",")[1];
            buffer.rewind();
            reply = buffer.toString();
            buffer.clear();
            
            message = "mutesrc " + WhiteNoiseID+ "," + "0";
            outStream.write((message +(char)3).getBytes());
            outStream.flush();
            replyLength = connectionReader.read(buffer);
            //String returnedID = buffer.split(",")[1];
            buffer.rewind();
            reply = buffer.toString();
            buffer.clear();
            
            message = "adjsrcgain" + WhiteNoiseID+ "," + "15";
            outStream.write((message +(char)3).getBytes());
            outStream.flush();
            replyLength = connectionReader.read(buffer);
            //String returnedID = buffer.split(",")[1];
            buffer.rewind();
            reply = buffer.toString();
            buffer.clear();
            
            message = "start";
            outStream.write((message +(char)3).getBytes());
            outStream.flush();
            replyLength = connectionReader.read(buffer);
            //String returnedID = buffer.split(",")[1];
            buffer.rewind();
            reply = buffer.toString();
            buffer.clear();
            
            
            
        } catch (IOException ex) {
            Logger.getLogger(SpatialServerClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
       
    }


}

