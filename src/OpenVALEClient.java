/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OpenVALEClient {
    private Socket connection;
    private InputStream inStream = null;
    private OutputStream outStream = null;
    private BufferedReader connectionReader;
    private boolean verbose = false;
    private final int ERRTIME = 2000;
    public enum NoiseType{
        wav,
        noise,
        asio
    }
    public class LocalizationResponse{
        public int speakerID;
        public float responseTime;
    }
            
            
    public OpenVALEClient(String ipAddress,int port) throws IOException{
        this(ipAddress,port,false);
    }
    
    public OpenVALEClient(String ipAddress,int port, boolean verb) throws IOException{
        verbose = verb;
        if(verbose)
            System.out.println("Connecting to server");
        try {
            connection = new Socket(ipAddress,port);
        } catch (IOException ex) {
            System.out.println("Failed create new socket.");
            return;
        }
        if (connection.isConnected()) {
            System.out.println("Connected");
            try {
                inStream = connection.getInputStream();
            } catch (IOException ex) {
                System.out.println("Failed to connect to input stream.");
                DisplayMessage("FATAL: FAILED TO CONNECT TO INPUT STREAM");
                System.exit(1);
                return;
            }
            try {
                outStream = connection.getOutputStream();
            } catch (IOException ex) {
                System.out.println("Failed to connect to output stream.");
                DisplayMessage("FATAL: FAILED TO CONNECT TO OUTPUT STREAM");
                System.exit(1);
                return;
            }

        } else {
            System.out.println("Failed to connect to server.");
            DisplayMessage("FATAL: FAILED TO CONNECT TO SERVER");
            System.exit(1);

        }
        connectionReader = new BufferedReader(new InputStreamReader(inStream));
        if(verbose)
            System.out.println("OpenVALE Server Setup Complete");
        
            
    }
    public void Disconnect() {
    
        try {
            connection.close();
        } catch (IOException ex) {
            Logger.getLogger(OpenVALEClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int LoadHRTF(String filename) throws IOException, InterruptedException{
    if(verbose)
            System.out.println("Calling LoadHRTF");
        outStream.write(("loadHRTF("+filename+")\n").getBytes());
        outStream.flush();
        
        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[2];
        int parsedInteger;
        try{
             parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to parse integer from " + returnedID);
            FailedParseError();
            return -1;
        }
        if(verbose)
            System.out.println("Completed loadHRTF adding " + filename + ", " + parsedInteger );
        
        return parsedInteger;
    }
    public int SetHRTF(String filename) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling SetHRTF");
        outStream.write(("setHRTF("+filename+")\n").getBytes());
        outStream.flush();
        
        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
             parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to parse integer from " + returnedID);
            FailedParseError();
            return -1;
        }
        if(verbose)
            System.out.println("Completed setHRTF adding " + filename);
        
        return parsedInteger;
    }
    public int SetHRTF(int hrtfID)throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling SetHRTF");
        outStream.write(("setHRTF("+hrtfID+")\n").getBytes());
        outStream.flush();
        
        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
             parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to parse integer from " + returnedID);
            FailedParseError();
            return -1;
        }
        if(verbose)
            System.out.println("Completed setHRTF with ID: " + hrtfID);
        return parsedInteger;

    }
    
    public int SetHRTF(int sourceNo, int hrtfID) throws IOException, InterruptedException {
        if(verbose)
            System.out.println("Calling SetHRTF");
        outStream.write(("setHRTF(" + sourceNo + "," + hrtfID +")\n").getBytes());
        outStream.flush();
        
        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
             parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to parse integer from " + returnedID);
            FailedParseError();
            return -1;
        }
        if(verbose)
            System.out.println("Completed setHRTF with ID: " + hrtfID);
        return parsedInteger;
    }

    public int AddAudioSource(NoiseType nType,ArrayList<Float> position,int id)throws IOException, InterruptedException{

        return AddAudioSource(nType,position, id,"");

    }


    public int AddAudioSource(NoiseType nType, ArrayList<Float> location,int id, String filename)throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling AddAudioSource");
        String message = "addAudioSource(";
        switch(nType){
            case noise:
                message += "noiseGen," + ("["+location.get(0)+ ","+location.get(1)+ ","+location.get(2)+ "],"+id+")");
                break;
            case wav:
                message += "wav," + "["+location.get(0)+ ","+location.get(1)+ ","+location.get(2)+ "],"+id+  "," +filename + ")";
                break;
            case asio:
                message += "asio," + "["+location.get(0)+ ","+location.get(1)+ ","+location.get(2)+ "],"+id+  ",6)";
                break;
        }

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        if(verbose)
            System.out.println(reply);
        System.out.println(reply.length());
        String returnedID = reply.split(",")[2];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to parse integer from " + returnedID);
            FailedParseError();
            return -1;
        }
        if(verbose) {
            switch (nType) {
                case noise:
                    System.out.println("Completed AddAudioSource with Noise Source");
                    break;
                case wav:
                    System.out.println("Completed AddAudioSource from filename: " + filename);
            }
        }
        return parsedInteger;

    }
    public int EnableSrc(int audioSourceID, boolean enable) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling EnableSrc");
        String message = "enableSrc(" + audioSourceID+ ",";
        if(enable)
            message += "T";
        else
            message += "F";
        message += ")";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        System.out.println(reply);
        String returnedID = reply.split(",")[1];
        returnedID = returnedID.trim();
        
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to enable source " + audioSourceID);
            DisplayMessage("ERROR: FAILED TO ENABLE SOURCE" + audioSourceID);
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("Completed enableSrc " + audioSourceID);
                    break;
                default:
                    System.out.println("Failed to complete enableSrc" + reply);
                    DisplayMessage("ERROR: FAILED TO ENABLE SOURCE");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int StartRendering() throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling StartRendering");
        String message = "StartRendering()";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to start rendering");
            DisplayMessage("ERROR: RENDERING FAILURE");
            Thread.sleep(ERRTIME);
            return -1;
        }
        System.out.println(reply);
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("Started Rendering");
                    break;
                default:
                    System.out.println("Failed to start rendering");
                    DisplayMessage("ERROR: RENDERING FAILURE");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int EndRendering() throws IOException, InterruptedException{
        if(verbose)
            System.out.println("EndRendering");
        String message = "EndRendering()";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to end rendering");
            DisplayMessage("ERROR: END RENDERING FAILURE");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("End Rendering");
                    break;
                default:
                    System.out.println("Failed to end rendering");
                    DisplayMessage("ERROR: END RENDERING FAILURE");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int Reset() throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Reset");
        String message = "Reset()";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed to complete reset");
            DisplayMessage("ERROR: FAILED RESET");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("Reset Complete");
                    break;
                default:
                    System.out.println("Failed to complete reset");
                    DisplayMessage("ERROR: FAILED RESET");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int DefineFront() throws IOException{

        String message = "defineFront()";
        System.out.println(message);
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        System.out.println(reply);
        return 1;

    }
    public int AdjustSourceLevel(int sourceID, float relativeLevel) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling AdjustSourceLevel");
        String message = "adjustSourceLevel("+sourceID+","+relativeLevel+")";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        System.out.println(sourceID + " " + reply);
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed adjustSourceLevel : " + sourceID + "," + relativeLevel);
            DisplayMessage("ERROR: FAILED ADJUSTSOURCELEVEL");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("adjustSourceLevel : " + sourceID + "," + relativeLevel);
                    break;
                default:
                    System.out.println("Failed adjustSourceLevel : " + sourceID + "," + relativeLevel);
                    DisplayMessage("ERROR: FAILED ADJUSTSOURCELEVEL");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int AdjustOverallLevel(float relativeLevel) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling AdjustOverallLevel");
        String message = "adjustOverallLevel("+relativeLevel+")";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed adjustOverallLevel : " + relativeLevel);
            DisplayMessage("ERROR: FAILED ADJUSTOVERALLLEVEL");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("adjustOverallLevel : " + relativeLevel);
                    break;
                default:
                    System.out.println("Failed adjustOverallLevel : " + relativeLevel);
                    DisplayMessage("ERROR: FAILED ADJUSTOVERALLLEVEL");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int AdjustSourcePosition(int sourceID, ArrayList<Float> location) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling AdjustSourcePosition");
        String message = "adjustSourcePosition("+sourceID+ ","+"["+location.get(0)+ ","+location.get(1)+ ","+location.get(2)+ "])";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        System.out.println(reply);
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed adjustSourcePosition : " + location.get(0)+ ","+location.get(1)+ ","+location.get(2));
            DisplayMessage("ERROR: FAILED ADJUSTSOURCEPOSITION");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("adjustSourcePosition : " +  location.get(0)+ ","+location.get(1)+ ","+location.get(2));
                    break;
                default:
                    System.out.println("Failed adjustSourcePosition : " + location.get(0)+ ","+location.get(1)+ ","+location.get(2));
                    DisplayMessage("ERROR: FAILED ADJUSTSOURCEPOSITION");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int AdjustSourcePosition(int sourceID, Integer speakerID) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling AdjustSourcePosition");
        String message = "adjustSourcePosition("+sourceID+ ","+speakerID+")";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed adjustSourcePosition : " +sourceID+ ","+speakerID);
            DisplayMessage("ERROR: FAILED ADJUSTSOURCEPOSITION");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("adjustSourcePosition : " +sourceID+ ","+speakerID);
                    break;
                default:
                    System.out.println("Failed adjustSourcePosition : " +sourceID+ ","+speakerID);
                    DisplayMessage("ERROR: FAILED ADJUSTSOURCEPOSITION");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }


    public int MuteAudioSource(int sourceID, boolean mute ) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling MuteAudioSource : " + sourceID);
        String message = "muteAudioSource("+sourceID+ ",";//+"["+location.get(0)+ ","+location.get(1)+ ","+location.get(2)+ "])";
        if(mute)
            message += "T)";
        else
            message += "F)";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed muteAudioSource : " + reply);
            DisplayMessage("ERROR: FAILED MUTEAUDIOSOURCE");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("muteAudioSource : " +  reply);
                    break;
                default:
                    System.out.println("Failed muteAudioSource : " + reply);
                    DisplayMessage("ERROR: FAILED MUTEAUDIOSOURCE");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }

    public int SetLEDs(int sourceID, ArrayList<Integer> mask ) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling SetLEDs");
        String message = "setLEDs("+sourceID+ "," + "["+mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3)+"])";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        
        catch(NumberFormatException e){
            System.out.println("Failed setLEDs : " + sourceID+ ","+  mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3));
            DisplayMessage("ERROR: FAILED SETLED");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("setLEDs : " +  sourceID+ ","+  mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3));
                    break;
                default:
                    System.out.println("Failed setLEDs : " + sourceID+ ","+  mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3));
                    DisplayMessage("ERROR: FAILED SETLED");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int SetLEDs(ArrayList<Float> location, ArrayList<Integer> mask ) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling SetLEDs");
        String message = "setLEDs(["+location.get(0) + ","+location.get(1) + ","+location.get(2) +"]," + "["+mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3)+"])";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        System.out.println("LED Reply " + reply);
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed setLEDs : " +location.get(0) + ","+location.get(1) + ","+location.get(2) + ","+  mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3)); 
            DisplayMessage("ERROR: FAILED SETLED");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("setLEDs : " +location.get(0) + ","+location.get(1) + ","+location.get(2) + ","+  mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3));
                    break;
                default:
                    System.out.println("Failed setLEDs : " +location.get(0) + ","+location.get(1) + ","+location.get(2) + ","+  mask.get(0)+ ","+mask.get(1)+ ","+mask.get(2)+ ","+mask.get(3));
                    DisplayMessage("ERROR: FAILED SETLED");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int ShowFreeCursor(String attachmentLocation, boolean on) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling ShowFreeCursor");

        String message = "showFreeCursor("+attachmentLocation+ ",";
        if(on)
            message+="T)";
        else
            message+="F)";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed showFreeCursor : " + attachmentLocation);
            DisplayMessage("ERROR: FAILEDSHOWFREECURSOR");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("Completed showFreeCursor : " + attachmentLocation);
                    break;
                default:
                    System.out.println("Failed showFreeCursor : " + attachmentLocation );
                    DisplayMessage("ERROR: FAILEDSHOWFREECURSOR");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int ShowSnappedCursor(String attachmentLocation, boolean on) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling ShowSnappedCursor");
        String message = "showSnappedCursor("+attachmentLocation+ ",";
        if(on)
            message+="T)";
        else
            message+="F)";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed showSnappedCursor : " + attachmentLocation);
            DisplayMessage("ERROR: FAILED SHOWSNAPPEDCURSOR");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("showSnappedCursor : " + attachmentLocation );
                    break;
                default:
                    System.out.println("Failed showSnappedCursor : " + attachmentLocation);
                    DisplayMessage("ERROR: FAILED SHOWSNAPPEDCURSOR");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public LocalizationResponse WaitforResponse() throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling WaitForResponse");
        String message = "waitForResponse()";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        System.out.println(reply);
        String returnedID = reply.split(",")[2].replaceAll("\\[","");
        String responseTime = reply.split(",")[3].replaceAll("]","");

        LocalizationResponse response = new LocalizationResponse();
        try{
            response.speakerID = Integer.parseInt(returnedID);
            response.responseTime = Float.parseFloat(responseTime);
        }
        catch(NumberFormatException e){
            System.out.println("Failed parse response from " + reply );
            DisplayMessage("ERROR: FAILED WAITFORRESPONSE");
            Thread.sleep(ERRTIME);
            return null;
        }
        if(verbose) {
            System.out.println("Localization response : " + response.speakerID + " , " + response.responseTime);
        }
        return response;

    }
    public int WaitForRecenter(int speakerID, float radiansTolerance) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling WaitForRecenter");
        String message = "waitForRecenter("+speakerID+ ","+radiansTolerance+")";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed waitForRecenter" );
            DisplayMessage("ERROR: FAILED WAITFORRECENTER");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("Completed waitForRecenter");
                    break;
                default:
                    System.out.println("Failed waitForRecenter");
                    DisplayMessage("ERROR: FAILED WAITFORRECENTER");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int WaitForRecenter(ArrayList<Float> location, float radiansTolerance) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling WaitForRecenter");
        String message = "waitForRecenter(["+location.get(0)+ ","+location.get(1)+ ","+location.get(2)+ "],"+radiansTolerance+")";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed waitForRecenter" );
            DisplayMessage("ERROR: FAILED WAITFORRECENTER");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("Completed waitForRecenter");
                    break;
                default:
                    System.out.println("Failed waitForRecenter");
                    DisplayMessage("ERROR: FAILED WAITFORRECENTER");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }


    public ArrayList<Float> GetHeadOrientation() throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling HeadOrientation");
        String message = "getHeadOrientation()";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        reply = reply.replaceAll("\\p{Alnum}*\\p{Punct}*\\[","");
        String xString = reply.split(",")[1];
        String yString = reply.split(",")[2];
        String zString = reply.split(",")[3].replaceAll("]","");
        float x;
        float y;
        float z;
        
        try{
            x = Float.parseFloat(xString);
            y = Float.parseFloat(yString);
            z = Float.parseFloat(zString);
        }
        catch(NumberFormatException e){
            System.out.println("Failed parse HeadOrientation from " + reply );
            DisplayMessage("ERROR: FAILED GETHEADORIENTATION");
            Thread.sleep(ERRTIME);
            return null;
        }
        if(verbose) {
            System.out.println("HeadOrientation : " + x +"," + y + "," +z);
        }
        ArrayList<Float> response = new ArrayList<>();
        response.add(x);
        response.add(y);
        response.add(z);
        return response;

    }
    public String SendNonStandardMessage(String message)throws IOException
    {String reply = "";
        try {
            outStream.write((message +"\n").getBytes());
        
        outStream.flush();

        reply = connectionReader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(OpenVALEClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  reply;
    }
    public ArrayList<Float> GetHead6DOF() throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling GetHead6DOF");
        String message = "GetHead6DOF()";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String xPosString = reply.split(",")[2].replaceAll("\\[","");
        String yPosString = reply.split(",")[3];
        String zPosString = reply.split(",")[4].replaceAll("]","");
        String xOriString = reply.split(",")[5].replaceAll("\\[","");
        String yOriString = reply.split(",")[6];
        String zOriString = reply.split(",")[7].replaceAll("]","");
        float xPos;
        float yPos;
        float zPos;

        float xOri;
        float yOri;
        float zOri;

        try{
            xPos = Float.parseFloat(xPosString);
            yPos = Float.parseFloat(yPosString);
            zPos = Float.parseFloat(zPosString);

            xOri = Float.parseFloat(xOriString);
            yOri = Float.parseFloat(yOriString);
            zOri = Float.parseFloat(zOriString);
        }
        catch(NumberFormatException e){
            System.out.println("Failed parse Head6DOF from " + reply );
            DisplayMessage("ERROR: FAILED GET6DOF");
            Thread.sleep(ERRTIME);
            return null;
        }
        if(verbose) {
            System.out.println("Head6DOF : " + xPos +"," + yPos + "," + zPos + "," + xOri +"," + yOri + "," + zOri);
        }
        ArrayList<Float> response = new ArrayList<>();
        response.add(xPos);
        response.add(yPos);
        response.add(zPos);
        response.add(xOri);
        response.add(yOri);
        response.add(zOri);
        return response;

    }
    public int GetNearestSpeaker(ArrayList<Float> location) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling GetNearestSpeaker");
        String message = "getNearestSpeaker(["+location.get(0)+ ","+location.get(1)+ ","+location.get(2)+ "])";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        String speakerID = reply.split(",")[2];
        int parsedInteger, speakerNumber;
        try{
            parsedInteger = Integer.parseInt(returnedID);
            speakerNumber = Integer.parseInt(speakerID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed getNearestSpeaker : "   + location);
            DisplayMessage("ERROR: FAILED GETNEARESTSPEAKER");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("Completed getNearestSpeaker : " + parsedInteger);
                    break;
                default:
                    System.out.println("Failed getNearestSpeaker : "   + location);
                    DisplayMessage("ERROR: FAILED GETNEARESTSPEAKER");
                    Thread.sleep(ERRTIME);
            }
        }
        return speakerNumber;

    }

    public ArrayList<Float> GetSpeakerPosition(int speakerID) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling GetSpeakerPosition");
        String message = "getSpeakerPosition("+speakerID+")";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String xString = reply.split(",")[2].replaceAll("\\[","");
        String yString = reply.split(",")[3];
        String zString = reply.split(",")[4].replaceAll("]","");
        float x;
        float y;
        float z;

        try{
            x = Float.parseFloat(xString);
            y = Float.parseFloat(yString);
            z = Float.parseFloat(zString);
        }
        catch(NumberFormatException e){
            System.out.println("Failed parse GetSpeakerPosition from " + reply );
            DisplayMessage("ERROR: FAILED GETSPEAKERPOSITION");
            Thread.sleep(ERRTIME);
            return null;
        }
        if(verbose) {
            System.out.println("GetSpeakerPosition : " + x +"," + y + "," +z);
        }
        ArrayList<Float> response = new ArrayList<>();
        response.add(x);
        response.add(y);
        response.add(z);
        return response;

    }
    public String GetCurrentHRTF(int speakerID) throws IOException{
        if(verbose)
            System.out.println("Calling GetCurrentHRTF");
        String message = "GetCurrentHRTF("+speakerID+")";
        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String response = reply.split(",")[1].replaceAll("\\[","");

        if(verbose) {
            System.out.println("GetCurrentHRTF : "+ reply);
        }
        return response;

    }
    public int HighlightLocation(ArrayList<Float> location, ArrayList<Float> color ) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling HighlightLocation");
        String message = "highlightLocation(["+location.get(0) + ","+location.get(1) + ","+location.get(2) +"]," + "["+color.get(0)+ ","+color.get(1)+ ","+color.get(2)+ "])";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        System.out.println(reply);
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed HighlightColor : " +location.get(0) + ","+location.get(1) + ","+location.get(2) + ","+  color.get(0)+ ","+color.get(1)+ ","+color.get(2));
            DisplayMessage("ERROR: FAILED HIGHLIGHTLOCATION");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("HighlightColor : " +location.get(0) + ","+location.get(1) + ","+location.get(2) + ","+  color.get(0)+ ","+color.get(1)+ ","+color.get(2));
                    break;
                default:
                    System.out.println("Failed HighlightColor : " +location.get(0) + ","+location.get(1) + ","+location.get(2) + ","+  color.get(0)+ ","+color.get(1)+ ","+color.get(2));
                    DisplayMessage("ERROR: FAILED HIGHLIGHTLOCATION");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int HighlightLocation(int spkID, ArrayList<Float> color ) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling HighlightLocation");
        String message = "highlightLocation("+spkID+"," + "["+color.get(0)+ ","+color.get(1)+ ","+color.get(2)+ "])";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        System.out.println(reply);
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed HighlightColor : " +spkID + ","+  color.get(0)+ ","+color.get(1)+ ","+color.get(2));
            DisplayMessage("ERROR: FAILED HIGHLIGHTLOCATION");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("HighlightColor : " +spkID + ","+  color.get(0)+ ","+color.get(1)+ ","+color.get(2));
                    break;
                default:
                    System.out.println("Failed HighlightColor : " +spkID+ ","+  color.get(0)+ ","+color.get(1)+ ","+color.get(2));
                    DisplayMessage("ERROR: FAILED HIGHLIGHTLOCATION");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int HighlightLocation( ) throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling HighlightLocation");
        String message = "highlightLocation()";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed HighlightColor");
            DisplayMessage("ERROR: FAILED HIGHLIGHTLOCATION");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("HighlightColor : ");
                    break;
                default:
                    System.out.println("Failed HighlightColor : ");
                    DisplayMessage("ERROR: FAILED HIGHLIGHTLOCATION");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    public int DisplayMessage(String displayMessage ) throws IOException{
        if(verbose)
            System.out.println("Calling DisplayMessage");
        String message = "displayMessage("+displayMessage+")";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed DisplayMessage : " + displayMessage);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("DisplayMessage : "+ displayMessage);
                    break;
                default:
                    System.out.println("Failed DisplayMessage : " + displayMessage);
            }
        }
        return parsedInteger;

    }
    
    public int GetSubjectNumber() throws IOException, InterruptedException{
        if(verbose)
            System.out.println("Calling GetSubjectNumber");
        String message = "GetSubjectNumber()";

        outStream.write((message +"\n").getBytes());
        outStream.flush();

        String reply = connectionReader.readLine();
        String returnedID = reply.split(",")[1];
        int parsedInteger;
        try{
            parsedInteger = Integer.parseInt(returnedID);
        }
        catch(NumberFormatException e){
            System.out.println("Failed GetSubjectNumber");
            DisplayMessage("ERROR: FAILED GETSUBJECTNUMBER");
            Thread.sleep(ERRTIME);
            return -1;
        }
        if(verbose) {
            switch (parsedInteger) {
                case 0:
                    System.out.println("GetSubjectNumber : " + reply);
                    break;
                default:
                    System.out.println("Failed GetSubjectNumber : " + reply);
                    DisplayMessage("ERROR: FAILED GETSUBJECTNUMBER");
                    Thread.sleep(ERRTIME);
            }
        }
        return parsedInteger;

    }
    
    private void FailedParseError() throws InterruptedException, IOException {
        DisplayMessage("ERROR: FAILED TO PARSE INTEGER");
        Thread.sleep(ERRTIME);
    }


}

    

