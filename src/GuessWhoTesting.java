/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GuessWhoTesting {

static int GoldenEarsID = 0;
static int DioticID = 0;
static int WhiteNoiseID = 0;
static int NoiseGeneratorID = 0;
static InputStream inStream = null;
static OutputStream outStream = null;
static OpenVALEClient client;
static int totalTrials = 10;
static int trialNum = 0;
static int spkID = 0;
static int numTests = 500;

static ArrayList<Integer> sourceList = new ArrayList<>();
public static void main(String[] args) throws IOException, InterruptedException {

        int count = 0;
        while (count < numTests){
            count++;
        System.out.println("Count " + count);
        client = new OpenVALEClient("127.0.0.1",43201,true);
        try {


            ArrayList<Float> center = new ArrayList<>();
            center.add(0.0f);
            center.add(-20.0f);
            center.add(0.0f);
            

           
            //System.out.println(client.SendNonStandardMessage("startaudioengine()"));    
            
            GoldenEarsID = client.LoadHRTF("GoldenClusterMean_Snow_SH6E150.slh");
            System.out.println(GoldenEarsID);
            
            //NoiseGeneratorID = client.AddAudioSource(OpenVALEClient.NoiseType.wav,center,GoldenEarsID,"richardnixoncheckers.wav");//client.AddAudioSource(OpenVALEClient.NoiseType.noise,center,GoldenEarsID);
            WhiteNoiseID = client.AddAudioSource(OpenVALEClient.NoiseType.noise,center,GoldenEarsID);
            //System.out.println(client.SendNonStandardMessage("startaudioengine()"));    
            
            client.StartRendering();
            client.ShowFreeCursor("hand", true);
            client.DefineFront();
            //client.EnableSrc(WhiteNoiseID  , true);
            //client.MuteAudioSource(WhiteNoiseID , false);
            System.out.println(client.SendNonStandardMessage("GuessWho(5,t,[1,0,0],16)"));
                
            client.Disconnect();
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(GuessWhoTesting.class.getName()).log(Level.SEVERE, null, ex);
                }
        
        }catch (java.io.IOException e){




            }
        }

    }
    static void playTrial(int srcID){
        try {

            client.EnableSrc(srcID, true);
            client.AdjustSourcePosition(WhiteNoiseID,spkID);
            client.MuteAudioSource(srcID, false);
            OpenVALEClient.LocalizationResponse response = client.WaitforResponse();
            client.MuteAudioSource(srcID, true);
            client.EnableSrc(srcID, false);
        }
        catch(Exception e){



        }


    }
}
