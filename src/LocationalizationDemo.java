/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jason.Ayers
 */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;




public class LocationalizationDemo {
    static int GoldenEarsID = 0;
static int DioticID = 0;
static int WhiteNoiseID = 0;
static int NoiseGeneratorID = 0;
static InputStream inStream = null;
static OutputStream outStream = null;
static OpenVALEClient client;
static int totalTrials = 10;
static int trialNum = 0;
static int spkID = 0;



public static void main(String[] args) throws IOException, InterruptedException {


        client = new OpenVALEClient("127.0.0.1",43201,true);
        try {


            ArrayList<Float> color = new ArrayList<>();
            color.add(1.0f);
            color.add(0.2f);
            color.add(0.2f);
            color.add(1.0f);
            GoldenEarsID = client.LoadHRTF("GoldenClusterMean_Snow_SH6E150.slh");
            WhiteNoiseID = client.AddAudioSource(OpenVALEClient.NoiseType.noise,color,GoldenEarsID);
            
            client.ShowSnappedCursor("hand", true);
            
            client.StartRendering();
            client.EnableSrc(WhiteNoiseID  , true);
            
            client.AdjustSourcePosition(WhiteNoiseID, 274);
            client.HighlightLocation(274, color);
            client.MuteAudioSource(WhiteNoiseID, false);
            
            client.WaitforResponse();
            client.MuteAudioSource(WhiteNoiseID, true);
            client.DefineFront();
            Random r = new Random();
            while(true){
            
                
            ArrayList<Float> position;
            do{
                spkID = r.nextInt(276)+1;
                position = client.GetSpeakerPosition(spkID);
            }while(position.get(2)<-.6f);
            client.AdjustSourcePosition(WhiteNoiseID, spkID);
            client.HighlightLocation(spkID, color);
            client.MuteAudioSource(WhiteNoiseID, false);
            
            client.WaitforResponse();
            client.MuteAudioSource(WhiteNoiseID, true);
            
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(LocationalizationDemo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }catch (java.io.IOException e){


        }
    }

    }

